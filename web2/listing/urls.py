from django.conf.urls import url, include
from .import views

urlpatterns = [
		url(r'^$', views.index,name='index'),
		url(r'^train/$', views.train,name='train'),
		url(r'^filter/(?P<states_id>[0-9]+)/$',views.second,name='second'),
		url(r'^filter/(?P<states_id>[0-9]+)/(?P<categories_id>[0-9]+)/$', views.third, name='third'),
		url(r'^filter/(?P<states_id>[0-9]+)/(?P<categories_id>[0-9]+)/(?P<places_id>[0-9]+)/$', views.fourth, name='fourth'),
		url(r'^filter/(?P<states_id>[0-9]+)/(?P<categories_id>[0-9]+)/$', views.authpage, name='authpage'),
		]
