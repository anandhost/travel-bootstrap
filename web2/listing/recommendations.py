from django.contrib.auth.models import User
from recommends.providers import RecommendationProvider
from recommends.providers import recommendation_registry

from .models import Places, Categories

class placerecommendation(RecommendationProvider):
    def get_user(self):
        return User.objects.filter(is_active=True, votes__isnull=False).distinct()
    def get_place(self):
        return Places.objects.get(id=places_id)
    def get_allplace(self):
        return Places.objects.filter(state=(selected_st),category=(selected_cat))
    def get_cat(self):
        return Categories.objects.get(pk=categories_id)

recommendation_registry.register(Categories, [Places], placerecommendation)
