from django.db import models
from django import forms
from ckeditor.fields import RichTextField

# Create your models here.
class States(models.Model):
   state_name=models.CharField(max_length=250)
   img=models.ImageField(upload_to = 'img/listing/',blank=True,null=True)
   aboutstate= RichTextField(blank=True)

   def __str__(self):
   		return self.state_name

class Categories(models.Model):
   #DEFAULT_ID=1
   #state=models.ForeignKey(States,default=DEFAULT_ID)
   category=models.CharField(max_length=100)
   img=models.ImageField(upload_to='img/listing/',blank=True,null=True)

   def __str__(self):
      return self.category

class Places(models.Model):
   DEFAULT_ID=1
   category= models.ForeignKey(Categories,default=DEFAULT_ID)
   state= models.ForeignKey(States,default=DEFAULT_ID)
   place_name= models.CharField(max_length=250)
   city= models.CharField(max_length=100)
   aboutp= RichTextField() #models.TextField(blank=True, null=True)
   img= models.ImageField(upload_to='img/listing/',blank=True,null=True)

   def __str__(self):
      return self.city + '-' + self.place_name

class About(models.Model):
    DEFAULT_ID=1
    category=models.ForeignKey(Categories,default=DEFAULT_ID)
    state=models.ForeignKey(States,default=DEFAULT_ID)
    pl=models.ForeignKey(Places,default=DEFAULT_ID)
    #tag=models.CharField(max_length=280,default=True)
    #about=models.TextField(blank=True,null=True)
    #img=models.ForeignKey(Places,default=DEFAULT_ID)

class Post(models.Model):
    DEFAULT_TITLE="No Title"
    title = models.CharField(max_length=100, default=DEFAULT_TITLE)
    content = RichTextField()

    def __str__(self):
      return self.title
