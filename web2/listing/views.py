from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render,  get_object_or_404
from .models import *
import authentication

def index(request):
	all_states = States.objects.all()
	context_dict ={'state':all_states}
	return render(request,'listing/index.html',context=context_dict)

def train(request):
	context={}
	return render(request,'listing/train.html')

def second(request,states_id):
	all_categories=Categories.objects.all()
	context_dict1={'category':all_categories,'states_id':states_id} #by me
	return render(request, 'listing/second.html',context=context_dict1)

def third(request,categories_id,states_id):
	selected_st=States.objects.get(id=states_id)
	selected_cat=Categories.objects.get(pk=categories_id)
	all_places=Places.objects.filter(state=(selected_st),category=(selected_cat))
	return render(request,'listing/third.html',{'place':all_places,"states_id":states_id,"categories_id":categories_id})

def fourth(request,categories_id,states_id,places_id):
	selected_st=States.objects.get(id=states_id)
	selected_cat=Categories.objects.get(pk=categories_id)
	selected_pl=Places.objects.get(id=places_id)
	all_places=Places.objects.filter(state=(selected_st),category=(selected_cat))
	all_ab=About.objects.filter(pl=selected_pl,state=selected_st,category=selected_cat)
	return render(request,'listing/fourth.html',{'about':all_ab,"states_id":states_id,"categories_id":categories_id,"place":selected_pl,"pl":all_places})

def authpage(request,categories_id,states_id):
	selected_st=States.objects.get(id=states_id)
	selected_cat=Categories.objects.get(pk=categories_id)
	all_places=Places.objects.filter(state=(selected_st),category=(selected_cat))
	return render(request,'authentication/success.html',{'place':all_places,"states_id":states_id,"categories_id":categories_id})

#def third(request,states_id,category_id):
	#category_id = request.GET.get("cat_id",1)
	#state_id = request.GET.get("state_id",1)
	#selected_cat= Categories.objects.get(pk=category_id)
	#all_places=Places.objects.filter(category__id=int(category_id),state__id=int(state_id))
	#return render(request,'listing/third.html',{'place':all_places})
	#try:
		#all_states=States.objects.get(pk=states_id)
		#all_categories=Categories.objects.get(pk=categories_id)
	#except States.DoesNotExist:
		#raise Http404("page does not exist")
#	return render(request,'listing/third.html',{'state':all_states},{'category',all_categories})
