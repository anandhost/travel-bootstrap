from django import forms
from django.contrib import admin
from listing.models import *
from ckeditor.widgets import CKEditorWidget
# Register your models here.
class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Post
        fields= "__all__"

class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm

admin.site.register(Post,PostAdmin)
admin.site.register(States)
admin.site.register(Categories)
admin.site.register(Places)


