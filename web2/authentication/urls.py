from django.conf.urls import url
from authentication import views
from authentication.views import *
import authentication.views as views
from django.contrib.auth.decorators import login_required



urlpatterns = [
	url(r'^login', views.Login.as_view(), name='login'),
	url(r'^register/', views.Register.as_view(), name='register'),
	url(r'success/', login_required(views.Success.as_view(), login_url='login'), name='success'),
    url(r'logout/$', views.Logout.as_view(), name='logout'),
	
]
